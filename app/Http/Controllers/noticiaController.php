<?php
namespace App\Http\Controllers;
use App\noticias;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class noticiaController extends Controller
{
    public function getNova() {
        return view('noticia.nova');
    }
    
    public function postNova(Requests\noticiaRequest $request){
        try {
            noticias::updateOrCreate([ "id"=> ($request->exists("id") ? $request->get("id") : -1) ], $request->except("_token"));
            return Redirect::back()->with("success", "Notícia Salva com Sucesso!");
        }catch (\Exception $e){
            return Redirect::back()->withErrors(["Não foi possível adicionar uma nova Notícia"]);
        }
    }
    
        public function getListar(){
        $noticias = noticias::with("User")->get();
        return view("noticia.listar", compact("noticias"));
    }
    
        public function getEditar($id){
        try {
            $noticia = noticias::find($id);
            return view("noticia.nova", compact("noticia"));
        }catch (\Exception $e){
            return Redirect::back()->withErros(["Não foi possível editar a notícia"]);
        }
    }
    public function getExcluir($id){
        try {
            $noticia = noticias::find($id);
            $noticia->delete();
            return Redirect::back()->with("success", "Notícia Excluída com Sucesso!");
        }catch (\Exception $e){
            return Redirect::back()->withErros(["Não foi possível excluir o Notícia"]);
        }
    }
    
    

    
}
