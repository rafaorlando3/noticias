<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class noticias extends Model
{
    protected $fillable = [ "titulo", "descricao", "user_id" ];
    function User(){
        return $this->hasOne(User::class, "id", "user_id");
    }
}
