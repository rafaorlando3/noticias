@extends("template")

@section("content")
@if (Route::has('login'))

<div class="flex-center position-ref full-height">
    <div class="top-right links">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
            Sair
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    @endif
    
      <div class="content">
        <div class="title m-b-md">
            Listando
        </div>
    <table border="1">
        <thead>
            <th>Título</th>
            <th>Descrição</th>
            <th>Autor</th>
             <th>Editar</th>
            <th>Excluir</th>
        </thead>
        <tbody>
            @foreach($noticias as $noticia)
                <tr>
                    
                    
                    <td>{{ $noticia->titulo }}</td>
                    <td>{!! $noticia->descricao !!}</td>
                    <td>{{ $noticia->User->name }}</td>
                   <td>
                       <a href="{{ route("noticia.editar", [ "id"=>$noticia->id ]) }}"><button>Editar</button></a>
                    </td>
                    <td> 
                        <a href="{{ route("noticia.excluir", [ "id"=>$noticia->id ]) }}"><button>Excluir</button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
          <br/>
          <a onclick="history.go(-1);" href="#">  <button > Voltar</button>
              </div>
</div>

@stop