@extends("template")

@section("content")
@if (Route::has('login'))
    <div class="flex-center position-ref full-height">
    <div class="top-right links">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
            Sair
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    @endif
    
      <div class="content">
        <div class="title m-b-md">
            Listando
        </div>
       <form method="POST" action="{{ route("noticia.nova") }}" enctype="multipart/form-data" >
        {{ csrf_field() }}
        @if(isset($noticia))
            <input type="hidden" name="id" value="{{ $noticia->id }}">
        @endif
        <div>
            <label>
                Título:
                <input type="text" name="titulo" value="{{ @$noticia->titulo }}">
            </label>
        </div>
        <div>
            <label>
                Conteúdo:
                <textarea name="descricao">
                    {!! @$noticia->descricao !!}
                </textarea>
            </label>
        </div>
        
        <div>
            <input type="submit" value="Salvar!">
        </div>
</form>
          
           <hr/>
          <a onclick="history.go(-1);" href="#">  <button > Voltar</button>
              </div>
</div>

@stop

@section("footer")
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@stop
