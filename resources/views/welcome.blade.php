@extends("template")

@section("content")
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
    <div class="top-right links">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
            Sair
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Notícias
        </div>

        <a href="{{ route('noticia.nova') }}">+Nova Notícia</a>
        <hr/>
        <a href="{{ route('noticia.listar') }}">+Listar</a>
    </div>
</div>
@stop
