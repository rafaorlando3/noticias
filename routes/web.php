<?php

Route::get('/', 'HomeController@index');



Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => '/Painel'], function() {

        Route::get('/', function () {
            return view('welcome');
        });

        Route::get('/Noticia/Listar', 'noticiaController@getListar')->name('noticia.listar');
        Route::get('/Noticia/Editar/{id}', 'noticiaController@getEditar')->name('noticia.editar');
        Route::get('/Noticia/Excluir/{id}', 'noticiaController@getExcluir')->name('noticia.excluir');
        Route::get('/Noticia/Nova', 'noticiaController@getNova')->name('noticia.nova');
        Route::post('/Noticia/Nova', 'noticiaController@postNova')->name('noticia.nova');
    });
});
